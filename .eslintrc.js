module.exports = {
  root: true,
  extends: '@react-native-community',
  "rules": {
    "prettier/prettier": "off",
    "no-spaced-func": "off",
    "comma-dangle": "off",
    "curly": "off"
  }
};
