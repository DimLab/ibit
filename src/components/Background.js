import React from 'react';
import {
	View,
	Dimensions,
	StyleSheet
} from 'react-native';
import { TitleBlock } from 'components';
import theme from 'theme';
const { height, width } = Dimensions.get ( 'window' );


export default React.memo ( Background );

function Background ({ title, right })
{
	return (
		<View style={ styles.container }>
			<View style={[ styles.square, right && styles.squareRigth ]}>
				<TitleBlock {...{ title, right }} />
				<View style={[ styles.side, right && styles.sideRight ]} />
			</View>
			<View style={[ styles.triangle, right && styles.triangleRight ]} />
		</View>
	);
}


const styles = StyleSheet.create ({
	container: {
		flex: 1,
		backgroundColor: theme.colors.white
	},
	square: {
		backgroundColor: theme.colors.yellow,
		height: theme.normalize ( 360 ),
		width: '100%'
	},
	squareRigth: {
		height: theme.normalize ( 540 )
	},
	triangle: {
		width: '100%',
		height: theme.normalize ( height ),
		backgroundColor: 'transparent',
		borderStyle: 'solid',
		borderLeftWidth: theme.normalize ( 200 ),
		borderTopWidth: theme.normalize ( 280 ),
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		borderTopColor: theme.colors.yellow
	},
	triangleRight: {
		borderRightWidth: theme.normalize ( 200 ),
		borderLeftWidth: undefined,
		borderTopWidth: theme.normalize ( 100 )
	},
	side: {
		width: theme.normalize ( 40 ),
		height: theme.normalize ( 80 ),
		borderWidth: 1,
		borderColor: theme.colors.grey,
		backgroundColor: theme.colors.lightGrey,
		position: 'absolute',
		left: 0,
		top: theme.normalize ( 240 )
	},
	sideRight: {
		right: 0,
		left: undefined
	}
});
