import React from 'react';
import {
	View,
	TouchableOpacity,
	Text,
	StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import theme from 'theme';

Icon.loadFont();


export default React.memo ( TabBar );

function TabBar ({ style, tabs = [], position, scrollTo })
{
	return (
		<View
			style={[ styles.container, theme.styles.shadow, style ]}
		>
			{ tabs.map ( ( { icon, label }, i ) => (
				<TouchableOpacity key={ icon + label } style={ styles.tab } onPress={ () => scrollTo ( i ) }>
					<Icon style={ position !== i && styles.inactive } name={ icon } size={ theme.normalize ( 24 ) } color="#000" />
					<Text style={[ styles.label, position !== i && styles.inactive ]}>{ label }</Text>
				</TouchableOpacity>
			) ) }
		</View>
	);
}


const styles = StyleSheet.create ({
	container: {
		height: theme.normalize ( 80 ),
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		backgroundColor: theme.colors.white,
		paddingBottom: theme.normalize ( 10 )
	},
	tab: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	inactive: {
		opacity: 0.3
	},
	label: {
		color: theme.colors.black,
		fontSize: theme.normalize ( 14 )
	}
});
