import React from 'react';
import {
	StyleSheet,
	View,
	Text,
	TextInput
} from 'react-native';
import theme from 'theme';


export default React.memo ( FormInput );

function FormInput ({ style, inputStyle, left, right, label, input, meta: { touched, error } = {}, ...rest })
{
	const invalid = !!( touched && error );

	return (
		<View style={[ styles.container, style ]}>
			{ !!label &&
				<Text style={[ theme.styles.label, right && styles.labelRight, styles.label ]}>{ label }</Text>
			}
			<TextInput
				style={[ theme.styles.formElement, left && theme.styles.left, right && theme.styles.right, styles.input, invalid && styles.error, inputStyle ]}
				{ ...input }
				{ ...rest }
			/>
			{ invalid &&
				<Text style={[ styles.errorText, right && styles.labelRight ]}>{ error }</Text>
			}
		</View>
	);
}

const styles = StyleSheet.create ({
	container: {
		paddingBottom: theme.normalize ( 20 )
	},
	label: {
		marginBottom: theme.normalize ( 10 )
	},
	labelRight: {
		marginLeft: theme.normalize ( 10 )
	},
	input: {
		borderWidth: 1,
		borderColor: theme.colors.lightGrey,
		color: theme.colors.grey,
		height: theme.normalize ( 60 ),
		width: '100%',
		paddingHorizontal: theme.normalize ( 20 )
	},
	error: {
		borderColor: theme.colors.red
	},
	errorText: {
		color: theme.colors.red,
		fontSize: theme.normalize ( 12 ),
		position: 'absolute',
		bottom: theme.normalize ( 4 )
	}
});
