import React from 'react';
import {
	TouchableOpacity,
	Text,
	StyleSheet
} from 'react-native';
import theme from 'theme';


export default React.memo ( PrimaryButton );

function PrimaryButton ({ style, title, left, right, ...rest })
{
	return (
		<TouchableOpacity
			style={[ styles.container, theme.styles.formElement, left && theme.styles.left, right && theme.styles.right, style ]}
			{ ...rest }
		>
			<Text style={ styles.title }>{ title.toUpperCase() }</Text>
		</TouchableOpacity>
	);
}


const styles = StyleSheet.create ({
	container: {
		height: theme.normalize ( 60 ),
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: theme.colors.yellow
	},
	title: {
		color: theme.colors.black,
		fontWeight: 'bold',
		fontSize: theme.normalize ( 13 ),
		letterSpacing: 1
	}
});
