import React from 'react';
import { Keyboard, TouchableWithoutFeedback } from 'react-native';


export default React.memo ( HideKeyboardWrapper );

function HideKeyboardWrapper ({ children })
{
	return (
		<React.Fragment>
			{ React.Children.map (
				children,
				( child, i ) => child === null ? null :
					<TouchableWithoutFeedback onPress={ Keyboard.dismiss }>{ React.cloneElement ( child ) }</TouchableWithoutFeedback>
			) }
		</React.Fragment>
	);
}
