import React from 'react';
import {
	View,
	Text,
	StyleSheet
} from 'react-native';
import theme from 'theme';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

Icon.loadFont();


export default React.memo ( TitleBlock );

function TitleBlock ({ style, title, right })
{
	return (
		<View style={ styles.container }>
			<Icon style={ styles.icon } name="tennis-ball" size={ theme.normalize ( 60 ) } color="#000" />
			<Text style={ styles.title }>{ title }</Text>
		</View>
	);
}


const styles = StyleSheet.create ({
	container: {
		paddingTop: theme.normalize ( 70 ),
		paddingLeft: theme.normalize ( 50 )
	},
	icon: {
		width: theme.normalize ( 60 ),
		transform: [ { rotateZ: '45deg' } ],
		marginLeft: theme.normalize ( -8 )
	},
	title: {
		color: theme.colors.black,
		fontSize: theme.normalize ( 24 ),
		marginTop: theme.normalize ( 8 )
	}
});
