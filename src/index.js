import React, { useRef, useState, useCallback } from 'react';
import {
	View,
	ScrollView,
	Dimensions,
	StyleSheet
} from 'react-native';
import ViewPager from '@react-native-community/viewpager';
import { Login, Registration } from 'screens';
import { TabBar } from 'components';
const { height } = Dimensions.get ( 'window' ),
	tabs = [
		{ icon: 'account-circle', label: 'Login' },
		{ icon: 'add-circle', label: 'Registration' }
	];


export default React.memo ( App );

function App ()
{
	const viewPager = useRef ( null ),
		[ position, setPosition ] = useState ( 0 ),
		onPageSelected = useCallback (
			e => setPosition ( e.nativeEvent.position ),
			[]
		),
		scrollTo = position => ( viewPager.current || {} ).setPage ( position );

	return (
		<View style={ styles.container }>
			<ViewPager
				ref={ viewPager }
				style={ styles.container }
				keyboardDismissMode="on-drag"
				initialPage={ position }
				{...{ onPageSelected }}
			>
				<Login />
				<Registration />
			</ViewPager>
			<TabBar {...{ tabs, position, scrollTo }} />
		</View>
	);
}


const styles = StyleSheet.create ({
	container: {
		flex: 1
	},
	content: {
		height: height - 80
	}
});
