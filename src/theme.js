import { Dimensions, StyleSheet } from 'react-native';
const { height } = Dimensions.get ( 'window' ),
	k = height / 896,
	normalize = val => Math.round ( k * val ),
	colors = {
		lightGrey: '#CCCCCC',
		grey: '#999999',
		yellow: '#FCD200',
		red: 'red',
		white: '#FFF',
		black: '#000'
	},
	shadow = {
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,

		elevation: 5
	};

export default {
	normalize,
	colors,
	styles: StyleSheet.create ({
		formElement: {
			borderRadius: normalize ( 10 )
		},
		label: {
			color: colors.grey,
			fontSize: normalize ( 15 )
		},
		left: {
			borderBottomRightRadius: 0,
			borderTopRightRadius: 0,
			borderRightWidth: 0
		},
		right: {
			borderBottomLeftRadius: 0,
			borderTopLeftRadius: 0,
			borderLeftWidth: 0
		},
		contentBlock: {
			justifyContent: 'space-between',
			backgroundColor: colors.white,
			paddingBottom: normalize ( 30 ),
			paddingTop: normalize ( 18 ),
			height: normalize ( 440 ),
			...shadow
		},
		shadow
	})
};
