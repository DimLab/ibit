import React from 'react';
import {
	View,
	TouchableOpacity,
	Text,
	Alert,
	StyleSheet
} from 'react-native';
import { HideKeyboardWrapper, FormInput, PrimaryButton } from 'components';
import { Field } from 'react-final-form';
import theme from 'theme';


export default React.memo ( LoginForm );

function LoginForm ({ style, invalid, handleSubmit })
{
	return (
		<HideKeyboardWrapper>
			<View style={[ theme.styles.formElement, theme.styles.left, theme.styles.contentBlock, styles.content, style ]}>
				<View>
					<Field
						name="username"
						component={ FormInput }
						left
						label="Username"
						keyboardType="email-address"
					/>
					<Field
						name="password"
						component={ FormInput }
						left
						label="Password"
						secureTextEntry
					/>
					<TouchableOpacity style={ styles.button } onPress={ () => Alert.alert ( '🤷🏼‍♂' ) }>
						<Text style={ theme.styles.label }>Forgot password?</Text>
					</TouchableOpacity>
				</View>
				<PrimaryButton
					left
					title="Login"
					onPress={ () => invalid && Alert.alert ( 'Validation error' ) || handleSubmit() }
				/>
			</View>
		</HideKeyboardWrapper>
	);
}


const styles = StyleSheet.create ({
	content: {
		paddingLeft: theme.normalize ( 30 )
	},
	button: {
		justifyContent: 'center',
		padding: theme.normalize ( 10 ),
		paddingLeft: 0
	}
});
