import React from 'react';
import {
	View,
	Alert,
	StyleSheet
} from 'react-native';
import { Form } from 'react-final-form';
import LoginForm from './LoginForm';
import { validateEmail } from 'functions';
import { Background } from 'components';
import theme from 'theme';
const isRequired = 'Field is required',
	initialValues = { username: '', password: '' },
	onSubmit = () => Alert.alert ( 'Authorized' );


export default React.memo ( Login );

function Login ({ position })
{
	return (
		<View style={ styles.container }>
			<Background title="Login to Continue..." />
			<Form
				style={ styles.form }
				component={ LoginForm }
				{...{ initialValues, onSubmit, validate }}
			/>
		</View>
	);
}


const styles = StyleSheet.create ({
	container: {
		flex: 1,
		backgroundColor: theme.colors.white
	},
	form: {
		position: 'absolute',
		right: 0,
		left: theme.normalize ( 40 ),
		top: theme.normalize ( 200 )
	}
});

function validate ({ username, password })
{
	const errors = {};

	if ( !username ) errors.username = isRequired;
	else if ( !validateEmail ( username ) ) errors.username = 'Invalid email';

	if ( !password ) errors.password = isRequired;
	else if ( password.length < 6 ) errors.password = 'Password too short';

	return errors;
}
