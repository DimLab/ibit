import React from 'react';
import {
	View,
	Alert,
	StyleSheet
} from 'react-native';
import { Form } from 'react-final-form';
import RegistrationForm from './RegistrationForm';
import { Background } from 'components';
import { validateEmail } from 'functions';
import theme from 'theme';
const isRequired = 'Field is required',
	initialValues = { username: '', password: '', password2: '' },
	onSubmit = () => Alert.alert ( 'Authorized' );


export default React.memo ( Registration );

function Registration ()
{
	return (
		<View style={ styles.container }>
			<Background right title="Registration" />
			<Form
				style={ styles.form }
				component={ RegistrationForm }
				{...{ initialValues, onSubmit, validate }}
			/>
		</View>
	);
}


const styles = StyleSheet.create ({
	container: {
		flex: 1,
		backgroundColor: theme.colors.white
	},
	form: {
		position: 'absolute',
		left: 0,
		right: theme.normalize ( 40 ),
		top: theme.normalize ( 200 )
	}
});

function validate ({ username, password, password2 })
{
	const errors = {};

	if ( !username ) errors.username = isRequired;
	else if ( !validateEmail ( username ) ) errors.username = 'Invalid email';

	if ( !password ) errors.password = isRequired;
	else if ( password.length < 6 ) errors.password = 'Password too short';

	if ( !password2 ) errors.password2 = isRequired;
	else if ( password2.length < 6 ) errors.password2 = 'Password too short';
	else if ( password2 !== password ) errors.password2 = 'Passwords must match';

	return errors;
}
