import React from 'react';
import {
	View,
	Alert,
	StyleSheet
} from 'react-native';
import { HideKeyboardWrapper, FormInput, PrimaryButton } from 'components';
import { Field } from 'react-final-form';
import theme from 'theme';


export default React.memo ( RegistrationForm );

function RegistrationForm ({ style, invalid, handleSubmit })
{
	return (
		<HideKeyboardWrapper>
			<View style={[ theme.styles.formElement, theme.styles.right, theme.styles.contentBlock, styles.content, style ]}>
				<View>
					<Field
						name="username"
						component={ FormInput }
						right
						label="Username"
						keyboardType="email-address"
					/>
					<Field
						name="password"
						component={ FormInput }
						right
						label="Password"
						secureTextEntry
					/>
					<Field
						name="password2"
						component={ FormInput }
						right
						label="Confirm password"
						secureTextEntry
					/>
				</View>
				<PrimaryButton
					right
					title="Registration"
					onPress={ () => invalid && Alert.alert ( 'Validation error' ) || handleSubmit() }
				/>
			</View>
		</HideKeyboardWrapper>
	);
}


const styles = StyleSheet.create ({
	content: {
		paddingRight: 30
	}
});
